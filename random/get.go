// Download files over http with go
// Default to stdout unless [-o outfile] has been specified
package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

var (
	outFile = flag.String("o", "", "Output File")
)

func usage() {
	fmt.Fprintf(os.Stderr, "usage: %s [-o outfile] url\n", os.Args[0])
	os.Exit(2)
}

func main() {
	log.SetPrefix("get ")
	flag.Usage = usage
	flag.Parse()

	args := flag.Args()
	if len(args) < 1 {
		usage()
	}

	var out *os.File
	if *outFile != "" {
		o, err := os.Create(*outFile)
		if err != nil {
			log.Fatal(err)
		}
		defer o.Close()
		out = o
	} else {
		out = os.Stdout
	}

	r, err := http.Get(args[0])
	if err != nil {
		log.Fatal(err)
	}
	defer r.Body.Close()
	_, err = io.Copy(out, r.Body)
	if err != nil {
		log.Fatal(err)
	}
}
