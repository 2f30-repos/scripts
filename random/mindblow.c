#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void
die(const char *s)
{
	fprintf(stderr, "%s: %s\n", s, strerror(errno));
	exit(EXIT_FAILURE);
}

int
main(int argc, char *argv[])
{
	int pflag, eflag;
	int fd;
	int c;
	unsigned char *base;
	struct stat buf;
	off_t i;
	int flag;
	int (*fn)(void);

	pflag = eflag = 0;
	while ((c = getopt(argc, argv, "pe")) != -1) {
		switch (c) {
		case 'p':
			pflag = 1;
			break;
		case 'e':
			eflag = 1;
			break;
		default:
			return EXIT_FAILURE;
		}
	}

	if (argc - optind != 1) {
		fprintf(stderr, "usage: %s [-pe] bin\n", *argv);
		return EXIT_FAILURE;
	}

	fd = open(argv[optind], O_RDONLY);
	if (fd < 0)
		die("open");

	if (fstat(fd, &buf) < 0)
		die("fstat");

	base = mmap(0, buf.st_size, PROT_EXEC | PROT_READ,
		    MAP_PRIVATE, fd, 0);
	if (base == MAP_FAILED)
		die("mmap");

	flag = 0;
	setbuf(stdout, NULL);
	if (pflag) {
		printf("char shellcode[] = \n\t\"");
		for (i = 0; i < buf.st_size; ++i) {
			if (i && !(i % 8)) {
				printf("\"\n");
				flag = 1;
			}
			if (flag) {
				printf("\t\"");
				flag = 0;
			}
			printf("\\x%02x", base[i]);
		}
		printf("\";\n");
	}

	if (eflag) {
		fn = (int (*)(void))base;
		fn();
	}

	if (munmap(base, buf.st_size) < 0)
		die("munmap");

	close(fd);

	return EXIT_SUCCESS;
}

