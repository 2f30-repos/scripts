#!/bin/sh

# prints iterative diffs for a given file in cvs
# unless count is given it defaults to three diffs in the past
# assumes all revision numbers start with '1.'
# needs: cvs

# which file
if test -z "$1"; then
    echo "Usage: $(basename $0) file [-count]" 1>&2
    exit 1
else
    FILE=$1
fi

# how many diffs
N=$(echo $2 | sed 's/[^0-9]//g')
test -n "$N" || N=3

# escape filename for use in grep/sed
ESC='s/[\/()[]$*.^|]/\\&/g'
F=$(basename $FILE | sed $ESC)
D=$(dirname $FILE)

# current revision
CUR=$(grep "^/$F/" $D/CVS/Entries \
    | sed "s|^/$F/1\.\([0-9]*\)/.*$|\1|")
if test -z "$CUR"; then
    echo "File not in CVS!" 1>&2
    exit 2
fi

# count is at most as large as the cur revision
test $N -le $CUR || N=$CUR

echo "HEAD is revision 1.$CUR"
echo "Performing $N diffs"

# perform diffs in pairs
while test $N -gt 0; do
    PREV=$(expr $CUR - 1)
    echo
    cvs diff -r1.$PREV -r1.$CUR $FILE
    CUR=$PREV
    N=$(expr $N - 1)
done
