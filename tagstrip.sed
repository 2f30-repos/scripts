# sed script for removing script tags
# from web pages for better offline viewing
# experience.

# instructions:
# save it as strip.sed or any other filename
# you like.

# usage:
# sed -f strip.sed file.html

s%<script.*>.*</script>%<!-- deleted -->%
/<script.*>.*/,/<\/script>/s/.*/<!-- deleted -->/
