#!/bin/sh

# wireframe puffy generator
# img from: http://www.openbsd.org/images/tshirt-23.gif
# needs: imagemagick

SRC=tshirt-23.gif
DST=puffy.png

COLOR=00ffff # tron like
COLOR=00ff00 # true green
GEOM=646x710 # with openbsd text
GEOM=646x610 # without the text

# prepare colors
GRAY=3f3f3f # this is the bg val after grayscale
COLORNEG=$(perl -e "printf \"%x\n\", 0xffffff - 0x$COLOR")

# prepare masks
convert -size $GEOM xc:#$GRAY gray.png
convert -size $GEOM xc:#$COLORNEG colorneg.png

# grayscale it
convert $SRC -colorspace gray -crop "$GEOM+1+1" puffy_gray.png
# apply gray mask until black background
composite puffy_gray.png gray.png -compose Minus puffy_black.png
# apply color mask to get foreground
composite puffy_black.png colorneg.png -compose Minus $DST

# remove temp files
rm gray.png colorneg.png puffy_gray.png puffy_black.png
