#!/bin/sh
# Clean up seemingly unused system libraries
#
# $Beard: cleanlibs,v 1.1 2014/01/08 23:04:40 alexander Exp $
#
# Copyright (c) 2014 Alexander Hall <alexander@beard.se>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

usage() {
	echo "usage: ${0##*/} [-n]" >&2
	exit 1
}

NOP=
OPTS=
PREFIX=/usr/lib/lib
PKGDB=/var/db/pkg
LIBCAT='@wantlib'

while getopts 'n' opt "$@" 2>/dev/null; do
	case $opt in
	n)	NOP=echo;;
	*)	usage;;
	esac
done

ls -1 $PREFIX*.so.* |
sed "s|$PREFIX||;s|\.so\.|.|" |
sort -r -t. -k1,1 -n -k2,3 |
awk -F. 'a[$1]++' |
grep -v -hf/dev/fd/3 3<<_ |
$(find $PKGDB -name +CONTENTS -type f -exec sed -n "s/^$LIBCAT //p" {} + | sort -u)
_
sort -u |
sed "s|\.|\.so\.|;s|^|$PREFIX|" |
xargs -r $NOP rm
