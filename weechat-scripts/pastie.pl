use strict;

my $SCRIPT_NAME = "pastie";

weechat::register("pastie", "cipher <haris\@2f30.org>", "0.1", "Public Domain", "Paste output", "", "");
weechat::hook_command("pastie", "Just select your text with the cursor, and \"/pastie\" in your window.
By default, it uses \"xsel -o\" to output X selection.

If you want another application for output, you can set it for example with:

  /set plugins.var.perl.pastie.ext_command \"xsel -o\"

Replace \"xsel -o\" with your program of choice.
----------------------------------------------", "", "", "", "pastie", "");

my $pastie_command;

init_config();

sub init_config {
  my %options = (
                  "ext_command" => "xsel -o",
  );
  foreach my $option (keys %options) {
    weechat::config_set_plugin($option, $options{$option}) unless weechat::config_is_set_plugin($option);
  }
}

sub pastie {
  my ($data, $command, $return_code, $out, $err) = @_;
  my $buffer = $data;
  $pastie_command = weechat::config_get_plugin("ext_command");
  my @output = `$pastie_command`;

  weechat::command($buffer, ",--<--");

  foreach my $output (@output) {
    $output =~ s/(.*?)\t/  /g;
    weechat::command($buffer, "| $output");
  }

  weechat::command($buffer, "`-->--");
  return weechat::WEECHAT_RC_OK;
}
