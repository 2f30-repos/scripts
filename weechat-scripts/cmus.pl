# weechat plugin to display current playing cmus track
use strict;

weechat::register("cmus", "cipher <haris\@2f30.org", "0.1", "Public Domain",
                  "displays cmus artist - song", "", "");
weechat::hook_command("cmus", "Script to display what cmus is currently playing. Just /cmus and enjoy.", "", "", "", "cmus", "");

sub cmus {
  my ($data, $buffer, $args) = @_;
  my $artistfull = qx(cmus-remote -Q | grep [[:space:]]artist[[:space:]]);
  chomp (my $artist = substr($artistfull,10));
  my $songfull = qx(cmus-remote -Q | grep [[:space:]]title[[:space:]]);
  chomp (my $song = substr($songfull,10));

  weechat::command($buffer, "/me cmus playing: $artist - $song");
  return weechat::WEECHAT_RC_OK;
}
