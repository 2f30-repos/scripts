# weechat script for url shorting
# Needed: WWW::Shorten::TinyURL
use strict;
use WWW::Shorten::TinyURL;
use Clipboard;

weechat::register("shorten", "cipher <haris\@2f30.org", "0.1", "Public Domain",
                  "Short url", "", "");
weechat::hook_command("shorten", "Press \"/shorten\" to output on the channel/buffer the url from clipboard in shortened form", "", "", "", "shorten", "");

sub shorten {
  my $external = Clipboard->paste;
  my ($data, $buffer, $args) = @_;
  my $short_url = makeashorterlink($external);
  weechat::command($buffer, "url: $short_url");
  return weechat::WEECHAT_RC_OK;
}
