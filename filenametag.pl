#!/usr/bin/perl
use File::Basename;
use File::Copy;
use MP3::Tag;

print "Filename Change according to TAG in mp3s <dsp@2f30.org>\n";
$num_args = $#ARGV + 1;
if ($num_args != 1) {
    print "\nUsage: filenametag.pl dir\n";
    die;
}

$dir = $ARGV[0];
print ":Opening Dir:\n";
opendir(DIR, $dir) or die "can't opendir $dir: $!\n";
while (@files = grep { /.mp3/ } readdir(DIR)){
    foreach $file (@files){
	$fname = $dir.'/'.$file;
	my $mp3 = MP3::Tag->new($fname);
	($title, $artist, $no, $album, $year) = $mp3->autoinfo();
	print "$title";
	if($file =~ m/$title/i){
	    print "\nall ok\n";
	}
	else{
	    $newfname = $dir.'/'.$title.'.mp3';
	    print "\ni will rename to $newfname!\n";
	    move($fname,$newfname);
	}
    }
}
closedir(DIR);
