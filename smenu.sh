#!/bin/sh

# Parses ~/.ssh/{config,known_hosts}. Fzf[0] takes over for fuzzy selection and
# passes argument to xargs.

set -A complete_ssh $(
	(awk '!/\*/ && /^Host /{print $2}' ~/.ssh/config;
	# awk '{sub(",.*$","",$1);print $1}' ~/.ssh/known_hosts
	)|sort -u
)

printf "%s\n" "${complete_ssh[@]}" | fzf | xargs -o ssh


# [0]: https://github.com/junegunn/fzf
