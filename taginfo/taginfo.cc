#include <iostream>
#include <iomanip>

#include <fileref.h>
#include <tag.h>

// dump tags of audio files
// depends: taglib

using namespace std;

static int all = 0;

int
main(int argc, char *argv[])
{
	if (argv[1] != NULL && strcmp(argv[1], "-a") == 0) {
		all = 1;
		argv++;
		argc--;
	}

	if (argc == 1) {
		cerr << "usage: taginfo [-a] file ..." << endl;
		return 1;
	}

	// detect unicode locale
	setlocale(LC_ALL, "");
	string lc = string(setlocale(LC_CTYPE, NULL));
	bool unicode = lc.find("UTF-8") != string::npos;

	for (int i = 1; i < argc; i++) {
		TagLib::FileRef f(argv[i]);
		TagLib::Tag *tag = f.tag();

		if (f.isNull() || !tag)
			continue;

		string a = tag->artist().toCString(unicode);
		string t = tag->title().toCString(unicode);
		string b = tag->album().toCString(unicode);
		uint y = tag->year();
		uint n = tag->track();
		string g = tag->genre().toCString(unicode);
		string c = tag->comment().toCString(unicode);

		cout << a;
		cout << " - ";
		cout << b;
		if (all) {
			cout << " (" << y << ")";
			cout << " [" << g << "]";
			cout << " -- " << c;
			cout << " -- " << setw(2) << n;
		}
		cout << " - ";
		cout << t;
		cout << endl;
	}

	return 0;
}
