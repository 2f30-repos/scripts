#!/bin/sh

# Needs sct. The following script changes color temperature.

while true
do
	if [ $(date '+%H') -gt 16 ] || [ $(date '+%H') -lt 10 ]
	then
		sct 4400
	else
		sct 5500
	fi
	sleep 60
done
