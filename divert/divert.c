/*
 * introduce artificial delay/packet drop in the networking stack
 * using divert sockets.
 *
 * pass out quick inet divert-packet port 666
 */

#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "arg.h"

char *argv0;
int port = 666;
int drop;
int wait;

void
ms2tv(struct timeval *tv, int ms)
{
	tv->tv_sec = ms / 1000;
	tv->tv_usec = (ms % 1000) * 1000;
}

int
tvsleep(struct timeval *tv)
{
	struct timespec ts;

	ts.tv_sec = tv->tv_sec;
	ts.tv_nsec = tv->tv_usec * 1000;
	return nanosleep(&ts, NULL);
}

void
loop(void)
{
	char pkt[65535];
	struct sockaddr_in sa;
	socklen_t salen;
	struct timeval tv;
	int fd, n;

	fd = socket(AF_INET, SOCK_RAW, IPPROTO_DIVERT);
	if (fd < 0)
		err(1, "socket");

	memset(&sa, 0, sizeof(sa));
	sa.sin_family = AF_INET;
	sa.sin_port = htons(port);
	sa.sin_addr.s_addr = 0;

	salen = sizeof(sa);
	if (bind(fd, (struct sockaddr *)&sa, salen) < 0)
		err(1, "bind");

	ms2tv(&tv, wait);
	for (;;) {
		tvsleep(&tv);
		n = recvfrom(fd, pkt, sizeof(pkt), 0,
		             (struct sockaddr *)&sa, &salen);
		if (n < 0) {
			warn("recvfrom");
			continue;
		}
		if (drop > arc4random_uniform(100))
			continue;
		n = sendto(fd, pkt, n, 0, (struct sockaddr *)&sa, salen);
		if (n < 0)
			warn("sendto");
	}
}

void
usage(void)
{
	fprintf(stderr, "usage: a.out [-d drop-percentage] [-i wait-interval] [-p port]\n");
	exit(1);
}

int
main(int argc, char *argv[])
{
	const char *errstr;

	ARGBEGIN {
	case 'd':
		drop = strtonum(EARGF(usage()), 0, 100, &errstr);
		if (errstr)
			errx(1, "invalid packet drop percentage");
		break;
	case 'i':
		wait = strtonum(EARGF(usage()), 1, 1000, &errstr);
		if (errstr)
			errx(1, "invalid timing interval");
		break;
	case 'p':
		port = strtonum(EARGF(usage()), 1, 65535, &errstr);
		if (errstr)
			errx(1, "invalid port");
		break;
	default:
		usage();
	} ARGEND
	if (argc)
		usage();
	loop();
}
