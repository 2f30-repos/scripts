#!/usr/bin/perl -w

# General use: unzip your file, head over to the directory where the *.html
# files exist and just execute the script. You'll find a nice "Previous/Next
# Page" link at the bottom of each page

use strict;
use 5.012;
use Cwd;

my $dir = getcwd;
my $filenames;
my @filenames = glob "$dir/*.html";

my $i = 0;

for ($i = 0; $i < $#filenames; $i++) {
  open my $fh, '>>', $filenames[$i] or die "Cound not open $filenames[$i]: $!";
  print $fh "<a href=\"$filenames[$i-1]\">Previous Page</a> <a href=\"$filenames[$i+1]\">Next Page</a>";
  close $fh;
}
