#!/bin/sh

# remember to change your external wireless interface
ext_if=ath0
# number of seconds to wait for interface to connect, adjust accordingly
time=4

usage() {
	echo "Usage: $0 [-h] [function1|function2]" 1>&2
}

downcmd() {
	echo "Disconnecting from current wireless"
	ifconfig $ext_if -nwid -wpakey -nwkey down
	sleep $time
}

upcmd() {
	echo "Connecting to new wireless"
	ifconfig $ext_if nwid $NWID wpakey $WPAKEY up
	sleep $time
}

function1() {
	NWID=ssid
	WPAKEY=pass
	downcmd
	upcmd
	dhclient $ext_if
}

function2() {
	NWID=otherssid
	WPAKEY=pass2
	downcmd
	upcmd
	dhclient $ext_if
}

# template() {
#	NWID=ssid
#	WPAKEY=pass
#	downcmd
#	upcmd
#	dhclient $ext_if
# }

if test -z "$1"
then
	usage
	exit 1
fi

while :
do
	case $1 in
		-h)
			usage
			exit 0
			;;
		function1)
			function1
			exit 0
			;;
		function2)
			function2
			exit 0
			;;
	esac
done
