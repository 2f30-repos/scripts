#include <stdio.h>

#include <mpd/client.h>

/* usage: mpdlen [list] */

int
main(int argc, char *argv[])
{
	struct mpd_connection *conn;
	struct mpd_song *song;
	unsigned secs = 0;
	char *list = argv[1];

	conn = mpd_connection_new(NULL, 0, 0);

	if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS)
		err(1, "mpd_connection_new");

	if (list != NULL)
		mpd_send_list_playlist_meta(conn, list);
	else
		mpd_send_list_queue_meta(conn);

	while ((song = mpd_recv_song(conn)) != NULL) {
		secs += mpd_song_get_duration(song);
		mpd_song_free(song);
	}

	mpd_connection_free(conn);

	printf("%02u:%02u:%02u\n", secs / 3600, secs % 3600 / 60, secs % 60);

	return 0;
}
